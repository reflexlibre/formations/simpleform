# Ce code permet de rendre un formulaire accessible sur le web et
# d'enregistrer les réponses.

# On importe plusieurs bibliothèques de codes
# flask: permet de créer des applications web
# csv: permet de manipuler les fichiers CSV (tableur)
from flask import Flask, render_template, request
import csv
import hashlib

# On créé l'application web
app = Flask(__name__)


# Le code contenu dans la fonction index() se déclenche avec la
# méthode 'GET' si on ouvre http://IP:5000/ dans un navigateur.
# Puis, il se déclenche avec la méthode POST si on clique pour
# envoyer les réponses du formulaire
@app.route('/', methods=['GET', 'POST'])
def index():

    # Si la méthode est GET
    if request.method == 'GET':
        # On affiche la page avec le formulaire
        return render_template('index.html')

    # Sinon (la méthode est POST)
    else:
        # Les réponses se trouvents dans request.form
        # On transforme les réponses en dictionnaire pour que ce soit
        # plus simple à manier
        data = request.form.to_dict(flat=True)

        # On récupère les noms des champs
        names = data.keys()

        # On ouvre le fichier answers.csv et on enregistre les réponses séparées par des virgules
        with open('answers.csv', 'a') as inFile:
            writer = csv.DictWriter(inFile, fieldnames=names)
            writer.writerow(data)

        # On affiche une page HTML en réponse
        render_template('merci.html')


# Le code contenu dans la fonction index() se déclenche avec la
# méthode 'GET' si on ouvre http://IP:5000/ dans un navigateur.
# Puis, il se déclenche avec la méthode POST si on clique pour
# envoyer les réponses du formulaire
@app.route('/login', methods=['GET', 'POST'])
def login():

    # Si la méthode est GET
    if request.method == 'GET':
        # On affiche la page avec le formulaire
        return render_template('login.html')

    # Sinon (la méthode est POST)
    else:
        data = request.form.to_dict(flat=True)
        hash_user = hashlib.md5(data['login'].encode()).hexdigest()
        if data['login'] == 'tom' and hash_user == "568194c6a8fb64c4928f2dd8f9b1777c":
            return render_template('member.html')
        else:
            return render_template('login.html')
